# LIBS ----
library(tidyverse)
library(data.table)
library(igraph)

# READ EDGES ----
e_sw_2002_ssh <- fread("data/outputs/network/e_sw_2002_ssh.csv")
e_sw_2002_stem <- fread("data/outputs/network/e_sw_2002_stem.csv")
e_sw_2012_ssh <- fread("data/outputs/network/e_sw_2012_ssh.csv")
e_sw_2012_stem <- fread("data/outputs/network/e_sw_2012_stem.csv")
e_sw_2022_ssh <- fread("data/outputs/network/e_sw_2022_ssh.csv")
e_sw_2022_stem <- fread("data/outputs/network/e_sw_2022_stem.csv")

e_en_2002_ssh <- fread("data/outputs/network/e_en_2002_ssh.csv")
e_en_2002_stem <- fread("data/outputs/network/e_en_2002_stem.csv")
e_en_2012_ssh <- fread("data/outputs/network/e_en_2012_ssh.csv")
e_en_2012_stem <- fread("data/outputs/network/e_en_2012_stem.csv")
e_en_2022_ssh <- fread("data/outputs/network/e_en_2022_ssh.csv")
e_en_2022_stem <- fread("data/outputs/network/e_en_2022_stem.csv")

e_ge_2002_ssh <- fread("data/outputs/network/e_ge_2002_ssh.csv")
e_ge_2002_stem <- fread("data/outputs/network/e_ge_2002_stem.csv")
e_ge_2012_ssh <- fread("data/outputs/network/e_ge_2012_ssh.csv")
e_ge_2012_stem <- fread("data/outputs/network/e_ge_2012_stem.csv")
e_ge_2022_ssh <- fread("data/outputs/network/e_ge_2022_ssh.csv")
e_ge_2022_stem <- fread("data/outputs/network/e_ge_2022_stem.csv")

# FUNCTION ----
quick_sna <- function(edge_list, file_name) {
  # Create igraph object
  net <- graph.data.frame(d = edge_list, directed = FALSE)
  
  # Print network statistics to the console
  # cat("Vertex count:", vcount(net), "\n")
  # cat("Edge count:", ecount(net), "\n")
  # cat("Edge density:", edge_density(net), "\n")

  # Calculate components and extract the largest connected component
  # cat("Number of components:", comps$no, "\n")

  net_lcc <- largest_component(net)

  # Print largest connected component statistics
  # cat("Largest connected component vertex count:", vcount(net_lcc),paste0("(", round(vcount(net_lcc)/vcount(net)*100, 2),"%",")"), "\n")
  # cat("Largest connected component edge count:", ecount(net_lcc),paste0("(", round(ecount(net_lcc)/ecount(net)*100, 2),"%",")"), "\n")
  # cat("Largest connected component edge density:", edge_density(net_lcc), "\n")

  # Calculate network properties
  V(net_lcc)$strength <- strength(net_lcc)
  V(net_lcc)$closeness <- closeness(net_lcc, normalized = TRUE)
  V(net_lcc)$betweenness <- betweenness(net_lcc, directed = FALSE, normalized = TRUE)
  V(net_lcc)$eigen <- eigen_centrality(net_lcc, directed = FALSE, scale = TRUE)$vector
  V(net_lcc)$ventile <- ntile(V(net_lcc)$strength, 20)
  # Run country/region from 1-wrangling.R
  V(net_lcc)$country <- node_country[V(net_lcc)$name]
  V(net_lcc)$region <- node_region[V(net_lcc)$name]

  # Prepare data for export
  net_lcc_export <- data.frame(
    Institution = V(net_lcc)$name,
    Region = V(net_lcc)$region,
    Country = V(net_lcc)$country,
    Strength = V(net_lcc)$strength,
    Between = V(net_lcc)$betweenness,
    Closeness = V(net_lcc)$closeness,
    Eigen = V(net_lcc)$eigen,
    Ventile = V(net_lcc)$ventile,
    stringsAsFactors = FALSE
  )

  # Use fwrite to write the data to a CSV file
  fwrite(net_lcc_export, file_name)
}

# NETWORK ----
## Sweden
#### 2002
quick_sna(e_sw_2002_stem, "data/outputs/netsw2002stem.csv")
quick_sna(e_sw_2002_ssh, "data/outputs/netsw2002ssh.csv")

#### 2012
quick_sna(e_sw_2012_stem, "data/outputs/netsw2012stem.csv")
quick_sna(e_sw_2012_ssh, "data/outputs/netsw2012ssh.csv")

#### 2022
quick_sna(e_sw_2022_stem, "data/outputs/netsw2022stem.csv")
quick_sna(e_sw_2022_ssh, "data/outputs/netsw2022ssh.csv")

## England
#### 2002
quick_sna(e_en_2002_stem, "data/outputs/neten2002stem.csv")
quick_sna(e_en_2002_ssh, "data/outputs/neten2002ssh.csv")

#### 2012
quick_sna(e_en_2012_stem, "data/outputs/neten2012stem.csv")
quick_sna(e_en_2012_ssh, "data/outputs/neten2012ssh.csv")

#### 2022
quick_sna(e_en_2022_stem, "data/outputs/neten2022stem.csv")
quick_sna(e_en_2022_ssh, "data/outputs/neten2022ssh.csv")

## Germany
#### 2002
quick_sna(e_ge_2002_stem, "data/outputs/netge2002stem.csv")
quick_sna(e_ge_2002_ssh, "data/outputs/netge2002ssh.csv")

#### 2012
quick_sna(e_ge_2012_stem, "data/outputs/netge2012stem.csv")
quick_sna(e_ge_2012_ssh, "data/outputs/netge2012ssh.csv")

#### 2022
quick_sna(e_ge_2022_stem, "data/outputs/netge2022stem.csv")
quick_sna(e_ge_2022_ssh, "data/outputs/netge2022ssh.csv")

beepr::beep(3)

# LCC ----
quick_lcc <- function(edge_list) {
  # Create igraph object
  net <- graph.data.frame(d = edge_list, directed = FALSE)
  
  net_lcc <- largest_component(net)

  # Calculate network properties
  V(net_lcc)$strength <- strength(net_lcc)
  V(net_lcc)$closeness <- closeness(net_lcc, normalized = TRUE)
  V(net_lcc)$betweenness <- betweenness(net_lcc, directed = FALSE, normalized = TRUE)
  V(net_lcc)$eigen <- eigen_centrality(net_lcc, directed = FALSE, scale = TRUE)$vector
  V(net_lcc)$ventile <- ntile(V(net_lcc)$strength, 20)
  # Run country/region from 1-wrangling.R
  V(net_lcc)$country <- node_country[V(net_lcc)$name]
  V(net_lcc)$region <- node_region[V(net_lcc)$name]
  
  return(net_lcc)
}

## Sweden
#### 2002
lcc_sw_stem_2002 <- quick_lcc(e_sw_2002_stem)
write.graph(lcc_sw_stem_2002,  #our network object
            file = "data/outputs/network/gephi/lcc_sw_stem_2002.graphml", #the path and name of our file
            format = "graphml") 
  
lcc_sw_ssh_2002 <- quick_lcc(e_sw_2002_ssh)
write.graph(lcc_sw_ssh_2002,  #our network object
            file = "data/outputs/network/gephi/lcc_sw_ssh_2002.graphml", #the path and name of our file
            format = "graphml") 
#### 2012
lcc_sw_stem_2012 <- quick_lcc(e_sw_2012_stem)
write.graph(lcc_sw_stem_2012,  #our network object
            file = "data/outputs/network/gephi/lcc_sw_stem_2012.graphml", #the path and name of our file
            format = "graphml") 

lcc_sw_ssh_2012 <- quick_lcc(e_sw_2012_ssh)
write.graph(lcc_sw_ssh_2012,  #our network object
            file = "data/outputs/network/gephi/lcc_sw_ssh_2012.graphml", #the path and name of our file
            format = "graphml") 
#### 2022
lcc_sw_stem_2022 <- quick_lcc(e_sw_2022_stem)
write.graph(lcc_sw_stem_2022,  #our network object
            file = "data/outputs/network/gephi/lcc_sw_stem_2022.graphml", #the path and name of our file
            format = "graphml") 

lcc_sw_ssh_2022 <- quick_lcc(e_sw_2022_ssh)
write.graph(lcc_sw_ssh_2022,  #our network object
            file = "data/outputs/network/gephi/lcc_sw_ssh_2022.graphml", #the path and name of our file
            format = "graphml") 

## England
#### 2002
lcc_en_stem_2002 <- quick_lcc(e_en_2002_stem)
write.graph(lcc_en_stem_2002,  #our network object
            file = "data/outputs/network/gephi/lcc_en_stem_2002.graphml", #the path and name of our file
            format = "graphml") 

lcc_en_ssh_2002 <- quick_lcc(e_en_2002_ssh)
write.graph(lcc_en_ssh_2002,  #our network object
            file = "data/outputs/network/gephi/lcc_en_ssh_2002.graphml", #the path and name of our file
            format = "graphml") 

#### 2012
lcc_en_stem_2012 <- quick_lcc(e_en_2012_stem)
write.graph(lcc_en_stem_2012,  #our network object
            file = "data/outputs/network/gephi/lcc_en_stem_2012.graphml", #the path and name of our file
            format = "graphml") 

lcc_en_ssh_2012 <- quick_lcc(e_en_2012_ssh)
write.graph(lcc_en_ssh_2012,  #our network object
            file = "data/outputs/network/gephi/lcc_en_ssh_2012.graphml", #the path and name of our file
            format = "graphml") 

#### 2022
lcc_en_stem_2022 <- quick_lcc(e_en_2022_stem)
write.graph(lcc_en_stem_2022,  #our network object
            file = "data/outputs/network/gephi/lcc_en_stem_2022.graphml", #the path and name of our file
            format = "graphml") 

lcc_en_ssh_2022 <- quick_lcc(e_en_2022_ssh)
write.graph(lcc_en_ssh_2022,  #our network object
            file = "data/outputs/network/gephi/lcc_en_ssh_2022.graphml", #the path and name of our file
            format = "graphml") 

## Germany
#### 2002
lcc_ge_stem_2002 <- quick_lcc(e_ge_2002_stem)
write.graph(lcc_ge_stem_2002,  #our network object
            file = "data/outputs/network/gephi/lcc_ge_stem_2002.graphml", #the path and name of our file
            format = "graphml") 

lcc_ge_ssh_2002 <- quick_lcc(e_ge_2002_ssh)
write.graph(lcc_ge_ssh_2002,  #our network object
            file = "data/outputs/network/gephi/lcc_ge_ssh_2002.graphml", #the path and name of our file
            format = "graphml") 

#### 2012
lcc_ge_stem_2012 <- quick_lcc(e_ge_2012_stem)
write.graph(lcc_ge_stem_2012,  #our network object
            file = "data/outputs/network/gephi/lcc_ge_stem_2012.graphml", #the path and name of our file
            format = "graphml") 

lcc_ge_ssh_2012 <- quick_lcc(e_ge_2012_ssh)
write.graph(lcc_ge_ssh_2012,  #our network object
            file = "data/outputs/network/gephi/lcc_ge_ssh_2012.graphml", #the path and name of our file
            format = "graphml") 

#### 2022
lcc_ge_stem_2022 <- quick_lcc(e_ge_2022_stem)
write.graph(lcc_ge_stem_2022,  #our network object
            file = "data/outputs/network/gephi/lcc_ge_stem_2022.graphml", #the path and name of our file
            format = "graphml") 

lcc_ge_ssh_2022 <- quick_lcc(e_ge_2022_ssh)
write.graph(lcc_ge_ssh_2022,  #our network object
            file = "data/outputs/network/gephi/lcc_ge_ssh_2022.graphml", #the path and name of our file
            format = "graphml") 
