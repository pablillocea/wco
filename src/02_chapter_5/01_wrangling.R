library(tidyverse)
library(data.table)
library(countrycode)

# READ ----
# list files
data <- list.files(path = "data/raw/rankings/universityrankings.ch",
                   full.names = TRUE)

# create reading function
read_plus <- function(flnm) {
  fread(flnm) |> 
    mutate_if(is.numeric, as.character) |> 
    mutate(filename = flnm) |> 
    mutate(filename = sub(".*ch/", "", filename)) |> 
    mutate(filename = sub(".csv", "", filename))
}

# create dataframe
DATA.raw <- map_dfr(data, read_plus) |> 
  select(-V4)

# CLEAN ----
DATA.clean <- DATA.raw |> 
  rename(
    "rank" = "#    World Rank",
    "institution" = "Institution",
    "country" = "Country"
  ) |> 
  separate(
    col = filename,
    into = c("evaluator", "year"),
    sep = "-"
  ) |> 
  separate(
    col = rank,
    into = c("rank", "range"),
    sep = "-"
  ) |> 
  mutate(rank = as.numeric(rank),
         evaluator = toupper(evaluator)) |> 
  as.data.frame()

fwrite(DATA.clean, "data/outputs/rankings/data_clean.csv")

# ADD REGION ----
DATA.geo <- DATA.clean |> 
  mutate(
    country = countrycode(sourcevar = country,
                          origin = "country.name.en.regex",
                          destination = "country.name.en"),
    region_a = countrycode(sourcevar = country,
                           origin = "country.name.en.regex",
                           destination = "region23"),
    region_b = countrycode(sourcevar = country,
                           origin = "country.name.en.regex",
                           destination = "continent"),
    region = case_when(
      region_b == "Americas" & region_a == "Northern America" ~ region_a,
      T ~ region_b
    )) |> 
  mutate(
    region = case_when(
      region == "Northern America" ~ "North America",
      T ~ region
    )
  ) |> 
  select(
    year,
    country,
    region,
    evaluator,
    rank
  )

# write out
fwrite(DATA.geo, "data/outputs/rankings/data_geo.csv")
