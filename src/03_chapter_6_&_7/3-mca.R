# LIBS ----
lapply(
  c(
    "data.table",
    "tidyverse",
    "FactoMineR",
    "countrycode",
    "factoextra",
    "explor",
    "extrafont"
  ),
  require,
  character.only = T
)

# FUNCTIONS ----
source("src/03_chapter_6_&_7/functions/benzecri_modified_rates.R")

# READ ----
rank_raw <- fread("data/outputs/rankings/rank_2022.csv")
labels <- fread("data/raw/ulabs.csv")

# FILTER & CLEAN ----
rank_clean <- rank_raw |>
  # keep obs featured in all three rankings
  filter(!is.na(arwu_rank) & !is.na(qs_rank) & !is.na(the_rank)) |> 
  # keep obs with rank <= 100 in at least one ranking
  filter(arwu_rank <= 100 | qs_rank <= 100 | the_rank <= 100) |> 
  relocate(region, .after = name) |>
  relocate(country, .after = region) |>
  # all scores to numeric
  mutate(
    across(
      17:21,
      function(x) {
        as.numeric(x)
      }
    ),
    `n&s` = case_when(
      is.na(`n&s`) ~ 0,
      TRUE ~ `n&s`
    )
  )

# RECODING INTO CATEGORICAL ----
rank_recoded <- rank_clean |>
  #group tiers
  mutate(
    across(
      c(arwu_rank,
        qs_rank,
        the_rank),
      function(x) {
        case_when(
          x <= 10 ~ "<= 10",
          x > 10 & x <= 20 ~ "> 10 & <= 20",
          x > 20 & x <= 30 ~ "> 20 & <= 30",
          x > 30 & x <= 40 ~ "> 30 & <= 40",
          x > 40 & x <= 50 ~ "> 40 & <= 50",
          x > 50 & x <= 60 ~ "> 50 & <= 60",
          x > 60 & x <= 70 ~ "> 60 & <= 70",
          x > 70 & x <= 80 ~ "> 70 & <= 80",
          x > 80 & x <= 90 ~ "> 80 & <= 90",
          x > 90 & x <= 100 ~ "> 90 & <= 100",
          T ~ "> 100")
      }
    )
  )|> 
  mutate(
    across(
      c(award,
        alumni
      ),
      function(x) {
        case_when(
          x == 0 ~ "no",
          TRUE ~ "yes"
        )
      }
    ),
    across(
      c(
        hici,
        `n&s`,
        pub,
        `academic reputation`,
        `employer reputation`,
        `faculty student`,
        `international faculty`,
        `international students`,
        teaching,
        citations,
        industry_income,
        international_outlook,
        research,
        pcp,
        `citations per faculty`
      ),
      function(x) {
        cut(x,
            breaks = quantile(x,
                              probs =
                                c(0, 0.20, 0.80, 1)
            ),
            labels = c("low", "mid", "high"),
            include.lowest = TRUE
        )
      }
    )
  )

# read and join supplementary
source("src/03_chapter_6_&_7/03_sup.R")

rank_ready <- rank_recoded |> 
  left_join(
    manual.he.gdp
  ) |> 
  # left_join(
  #   oecd.he.hh
  # ) |> 
  left_join(
    oecd.rd
  ) |> 
  left_join(
    sup.type
  )

# prepare MCA ready object
rank_mca <- rank_ready |> 
  select(
    -c(
      1:3,
      12:14,
      24
    )
  ) |> 
  mutate(
    region.b = case_when(
      country %in% c("United Kingdom", "Ireland", "Australia",
                     "New Zealand", "Singapore", "Hong Kong SAR China",
                     "Malaysia", "Israel") ~ "Anglo-Saxon",
      country %in% c("Belgium", "Denmark", "Finland", "France", "Germany",
                     "Netherlands", "Norway", "Sweden",
                     "Switzerland") ~ "Europe-no-AS",
      country %in% c("China", "Japan", "South Korea", "Taiwan", "Russia") ~ "Asia-no-AS",
      TRUE ~ region
    ),
    reg_age = paste0(region, "_",age)
  ) |> 
  rename(
    staffAw = award,
    alumniAw = alumni,
    academicRep = `academic reputation`,
    employRep = `employer reputation`,
    cit = `citations per faculty`,
    facStu = `faculty student`,
    intlFac = `international faculty`,
    intlStu = `international students`,
    intlOut = `international_outlook`,
    industry = `industry_income`
  ) |> 
  column_to_rownames("name")

# MCA ----
res_mca <- MCA(rank_mca,
               ncp = 24,
               quali.sup = c(1:5,
                             19:24),
               graph = T)

mca_eig <- as.data.frame(res_mca$eig) |> 
  rownames_to_column("dim")
mca_ben <- benzecri.rates(res_mca) |> 
  rownames_to_column("dim")

mca_sum <- mca_eig |> 
  left_join(
    mca_ben
  ) |> 
  select(
    dim,
    eigenvalue,
    variance = `percentage of variance`,
    `importance index` = mrate,
    `cummulative` = cum.mrate
  )

mca_sum

# CONTRIBUTIONS ----
ctr <- data.frame(res_mca$var$contrib[,1:4],
                  res_mca$var$coord[,1:4])

ctr_var <- ctr |> 
  rownames_to_column("id") |> 
  separate(id, c("cat", "mod"), "_") |> 
  group_by(cat) |> 
  summarise(
    d1 = sum(Dim.1),
    d2 = sum(Dim.2),
    d3 = sum(Dim.3),
    d4 = sum(Dim.4),
  )

ctr |> 
  rownames_to_column("id") |> 
  separate(id, c("cat", "mod"), "_") |> 
  filter(Dim.4 >= 2) |> 
  select(
    cat,
    mod,
    Dim.4,
    Dim.4.1
  ) |> 
  view()

ctr.ax.1 <- data.frame(
  coord = res_mca$var$coord[,1],
  ctr = res_mca$var$contrib[,1]
) |> 
  # filter(
  #   ctr >= mean(ctr) # 2.7
  # ) |> 
  rownames_to_column(
    "mod"
  ) |> 
  mutate(
    axis = "Axis 1"
  )

ctr.ax.2 <- data.frame(
  coord = res_mca$var$coord[,2],
  ctr = res_mca$var$contrib[,2]
) |> 
  # filter(
  #   ctr >= mean(ctr) # 2.7
  # ) |> 
  rownames_to_column(
    "mod"
  ) |> 
  mutate(
    axis = "Axis 2"
  )

ctr.ax.3 <- data.frame(
  coord = res.mca$var$coord[,3],
  ctr = res.mca$var$contrib[,3]
) |> 
  filter(
    ctr >= mean(ctr) # 2.7
  ) |> 
  rownames_to_column(
    "mod"
  ) |> 
  mutate(
    axis = "Axis 3"
  )

ctr.ax.4 <- data.frame(
  coord = res_mca$var$coord[,4],
  ctr = res_mca$var$contrib[,4]
) |> 
  rownames_to_column(
    "mod"
  ) |> 
  filter(
    str_detect(mod, "academic") | str_detect(mod, "employ") |
      str_detect(mod, "research") | str_detect(mod, "pcp") |
      str_detect(mod, "Stu") | str_detect(mod, "Out")
  )

combined_ctr <- bind_rows(ctr.ax.1, ctr.ax.2, ctr.ax.3, ctr.ax.4) |> 
  arrange(axis, desc(ctr)) |> 
  mutate(Subgroup = if_else(coord < 0, "Negative Coordinates", "Positive Coordinates"),
         mod = paste0(mod," (",round(ctr,2),"%)"))



# explor(res.mca)

# DATA EXTRACTION FOR VIS ----
# categories' coordinates
var_coord <- data.frame(res_mca$var[1:2]) |> 
  select(1:4, 25:28) |> 
  rownames_to_column("Name") |> 
  mutate(
    Name = case_when(
      str_starts(Name, "industry_income_") ~ str_replace(Name, "_", " "),
      str_starts(Name, "international_outlook_") ~ str_replace(Name, "_", " "),
      T ~ Name
    )
  ) |> 
  separate(
    Name, 
    c("var","mod"),
    sep = "_"
  ) |> 
  # mutate(
  #   var = case_when(
  #     var == "pcp" ~ "PCP",
  #     var == "academicRep" ~ "ARep",
  #     var == "citations per faculty" ~ "Cit",
  #     var == "employer reputation" ~ "ERep",
  #     var == "faculty student" ~ "F/S",
  #     var == "international faculty" ~ "IntF",
  #     var == "international students"~ "IntS",
  #     var == "teaching" ~ "Tch",
  #     var == "research" ~ "Rch",
  #     var == "industry income" ~ "IIn",
  #     var == "international outlook" ~ "IntO",
  #     TRUE ~ var
  #   )
  # ) |> 
  group_by(var) |> 
  mutate(
    v.ctr.1 = sum(unique(contrib.Dim.1)),
    v.ctr.2 = sum(unique(contrib.Dim.2)),
    v.ctr.3 = sum(unique(contrib.Dim.3)),
    v.ctr.4 = sum(unique(contrib.Dim.4)),
  ) |> 
  ungroup() |> 
  mutate(
    weight = case_when(
      var == "alumniAw" & mod == "yes" ~ 7,
      var == "alumniAw" & mod == "no" ~ 4,
      var == "staffAw" & mod == "yes" ~ 6.5,
      var == "staffAw" & mod == "no" ~ 5,
      mod == "high" ~ 3,
      mod == "mid" ~ 6,
      mod == "low" ~ 3
    )
  )

# individuals' coordinates
ind_coord <- data.frame(res_mca$ind[1:2]) |> 
  select(1:4, 25:28) |> 
  rownames_to_column("Name")|> 
  left_join(labels)


sup_coord <- data.frame(res_mca$quali.sup[1:2]) |> 
  #select(1:3, 21:23) |> 
  rownames_to_column("Name")

# CLUSTERING ----
res_hcpc <- HCPC(res_mca, graph = T, nb.clust = 7)

# extract clusters
clust <- as.data.frame(res_hcpc$data.clust) |>
  select(clust) |> 
  rownames_to_column("Name")

clust_data <- ind_coord |> 
  left_join(clust)

# centroids
centroids <- clust_data |> 
  group_by(clust) |> 
  summarise(centroid_1 = mean(coord.Dim.1), 
            centroid_2 = mean(coord.Dim.2),
            centroid_3 = mean(coord.Dim.3))

# dendogram

# clusters <- hclust(dist(ind.coord), # data matrix
#                    method = "ward.D" # Linkage method
# )

# VISUALISATION ----

# library(dendextend)
# 
# dend <- as.dendrogram(clusters)
# # Determine the height at which to cut the dendrogram to obtain seven clusters
# cut_dend <- cut(dend, h = 5.47)
# 
# # Get the dendrogram object for the clusters formed above the cut
# truncated_dend <- cut_dend$upper
# current_labels <- labels(truncated_dend)
# new_labels <- str_replace(current_labels, "Branch", "Cluster")
# labels(truncated_dend) <- new_labels
# 
# # Plot the truncated dendrogram
# par(mfrow = c(1,1))
# plot(truncated_dend, ylim = c(.5, max(heights_per_k.dendrogram(truncated_dend)["1"])),
#      family = "fira")
# abline(h = 5.47, col = "black", lty = 2, lwd = 1)
# 
# # extract cluster
# u.cluster <- res.hcpc$data.clust |>
#   rownames_to_column("name") |>
#   select(name,clust)
# 
# data.ready |> left_join(u.cluster) |> filter(clust == 1) |> pull("arwu_name") |> view()
# 
# # view cluster data
res_hcpc$data.clust |> view()
# 
# view()
# 
# res.hcpc$desc.var$test.chi2
c1 <- as.data.frame(res_hcpc$desc.var$category$`1`[,1:3]) |> 
  rownames_to_column("modality") |> view()
  fwrite("c1.csv")

c2 <- as.data.frame(res_hcpc$desc.var$category$`2`[,1:3]) |> 
  rownames_to_column("modality") |> view()
  fwrite("c2.csv")

c3 <- as.data.frame(res_hcpc$desc.var$category$`3`[,1:3]) |> 
  rownames_to_column("modality") |> view()
  fwrite("c3.csv")

c4 <- as.data.frame(res_hcpc$desc.var$category$`4`[,1:3]) |> 
  rownames_to_column("modality") |> view()
  fwrite("c4.csv")

c5 <- as.data.frame(res_hcpc$desc.var$category$`5`[,1:3]) |> 
  rownames_to_column("modality") |> view()
  fwrite("c5.csv")

c6 <- as.data.frame(res_hcpc$desc.var$category$`6`[,1:3]) |> 
  rownames_to_column("modality") |> view()
  fwrite("c6.csv")

c7 <- as.data.frame(res_hcpc$desc.var$category$`7`[,1:3]) |> 
  rownames_to_column("modality") |> view()
  fwrite("c7.csv")

res_hcpc$desc.axes
# 
# res.hcpc$desc.var$category[[3]][,1:3]
# data.ready |> left_join(u.cluster) |> filter(clust == 3) |> pull("arwu_name") |> view()

# COUNT ----
calculate_percentages <- function(data, category) {
  data %>%
    count(!!sym(category)) %>%
    mutate(Percentage = n / sum(n) * 100) %>%
    select(!!sym(category), Percentage) %>%
    rename("Modality" = 1)
}

