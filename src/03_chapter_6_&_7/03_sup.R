# LIBS ----
lapply(
  c(
    "data.table",
    "tidyverse",
    "countrycode"
  ),
  require,
  character.only = T
)

# MANUAL ----
# Data comes from OECD education at a glance 2022
# China and Russia from https://stats.moe.gov.tw/files/ebook/International_Comparison/2022/i2022_4-1.xls
# Switzerland from https://higheredstrategy.com/wp-content/uploads/2022/03/Switzerland.pdf
# Malaysia from https://tradingeconomics.com/malaysia/government-expenditure-on-tertiary-education-as-percent-of-gdp-percent-wb-data.html
# Singapore from https://tradingeconomics.com/singapore/government-expenditure-on-tertiary-education-as-percent-of-gdp-percent-wb-data.html
manual.he.gdp <- fread("data/raw/supplementary/manual_gdp_he.csv") |> 
  mutate(
    gdp_he = cut(gdp_he,
                 breaks = quantile(gdp_he,
                                   probs =
                                     c(0, 0.2, 0.4, 0.6, 0.8, 1)
                 ),
                 labels = c("lowest", "lower", "mid", "higher", "highest"),
                 include.lowest = TRUE
    )
  )

# UNESCO ----
# http://data.uis.unesco.org/index.aspx?queryid=3852
# for China https://www.ndc.gov.tw/Content_List.aspx?n=370BD5BCD05AD77F
# unesco.he.gdp <- fread("data/raw/supplementary/unesco-gdp-he.csv") |> 
#   filter(Indicator == "Government expenditure on tertiary education as a percentage of GDP (%)") |> 
#   group_by(LOCATION) |> 
#   arrange(desc(TIME)) |> 
#   slice(1) |> 
#   ungroup() |> 
#   mutate(
#     country = countrycode(
#       sourcevar = LOCATION,
#       origin = "iso3c",
#       destination = "country.name.en"
#     )
#   ) |> 
#   select(
#     country, 
#     gdp_he = Value
#     ) |> 
#   add_row(
#     country = "China",
#     gdp_he = 1.2
#   )

# OECD ----
# spending HE % GDP
# source https://data.oecd.org/eduresource/public-spending-on-education.htm#indicator-chart
# OECD (2023), Public spending on education (indicator). doi: 10.1787/f99b45d0-en (Accessed on 19 October 2023)
oecd.he.gdp <- fread("data/raw/supplementary/oecd-spending-gdp-he.csv") |> 
  group_by(LOCATION) |> 
  arrange(desc(TIME))  |> 
  # keep most recent data
  slice(1) |> 
  # append country for later join
  ungroup() |> 
  mutate(
    country = countrycode(
      sourcevar = LOCATION,
      origin = "iso3c",
      destination = "country.name.en"
    )
) |> 
  # keep relevant variables and rows
  select(
    country, gdp_he = Value
  ) |> 
  filter(!is.na(country)) |> 
  # recode gdp_he
  mutate(
    gdp_he = cut(gdp_he,
                 breaks = quantile(gdp_he,
                                   probs =
                                     c(0, 0.2, 0.4, 0.6, 0.8, 1)
                 ),
                 labels = c("lowest", "lower", "mid", "higher", "highest"),
                 include.lowest = TRUE
    )
  )

# shares of HE spending
# source https://data.oecd.org/eduresource/spending-on-tertiary-education.htm
# OECD (2023), Spending on tertiary education (indicator). doi: 10.1787/a3523185-en (Accessed on 19 October 2023)
oecd.he.hh <- fread("data/raw/supplementary/oecd-spending-he.csv") |> 
  group_by(LOCATION) |> 
  arrange(desc(TIME))  |> 
  # use household % as proxi for fees
  filter(SUBJECT == "HH") |> 
  # keep most recent data
  slice(1) |> 
  # append country for later join
  ungroup() |> 
  mutate(
    country = countrycode(
      sourcevar = LOCATION,
      origin = "iso3c",
      destination = "country.name.en"
    )
  ) |> 
  # keep relevant variables and rows
  select(
    country, hh_he = Value
  ) |> 
  filter(!is.na(country)) |> 
  # recode hh_he
  mutate(
    hh_he = cut(hh_he,
                 breaks = quantile(hh_he,
                                   probs =
                                     c(0, 0.2, 0.4, 0.6, 0.8, 1)
                 ),
                 labels = c("lowest", "lower", "mid", "higher", "highest"),
                 include.lowest = TRUE
    )
  )

# spending R&D
# source https://data.oecd.org/rd/gross-domestic-spending-on-r-d.htm#indicator-chart
oecd.rd <- fread("data/raw/supplementary/oecd-spending-rd.csv") |> 
  group_by(LOCATION) |> 
  arrange(desc(TIME))  |> 
  # keep most recent data
  slice(1) |> 
  # append country for later join
  ungroup() |> 
  mutate(
    country = countrycode(
      sourcevar = LOCATION,
      origin = "iso3c",
      destination = "country.name.en"
    )
  ) |> 
  # keep relevant variables and rows
  select(
    country, gdp_rd = Value
  ) |> 
  filter(!is.na(country)) |> 
  # recode gdp_rd
  mutate(
    gdp_rd = cut(gdp_rd,
                 breaks = quantile(gdp_rd,
                                   probs =
                                     c(0, 0.2, 0.4, 0.6, 0.8, 1)
                 ),
                 labels = c("lowest", "lower", "mid", "higher", "highest"),
                 include.lowest = TRUE
    )
  )

# AGE, TYPE & LABEL ----
sup.type <- fread("data/raw/supplementary/top_sup.csv") |> 
  # age
  arrange(desc(established)) |> 
  mutate(
    age = cut(established,
                 breaks = quantile(established,
                                   probs =
                                     c(0, 0.2, 0.4, 0.6, 0.8, 1)
                 ),
                 labels = c("oldest", "older", "mid", "younger", "youngest"),
                 include.lowest = TRUE
    )
  ) |> 
  # type
  mutate(
    utype = case_when(
      type == "Comprehensive university" & uhospital == "yes" & legal_status == "private" ~ "comp hosp pri",
      type == "Comprehensive university" & uhospital == "yes" & legal_status == "public" ~ "comp hosp pub",
      type == "Comprehensive university" & uhospital == "no" & legal_status == "private" ~ "comp pri",
      type == "Comprehensive university" & uhospital == "no" & legal_status == "public" ~ "comp pub",
      str_starts(type, "Science,") & legal_status == "private" ~ "special pri",
      str_starts(type, "Science,") & legal_status == "public" ~ "special pub",
      str_starts(type, "Social") & legal_status == "private" ~ "special pri",
      str_starts(type, "Social") & legal_status == "public" ~ "special pub"
    )
  ) |> 
  # keep relevant variables
  select(
    name,
    age,
    utype
  )
