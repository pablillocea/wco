# LIBS ----
library(data.table)
library(tidyverse)

# IMPORT ----
RANK.raw <- fread("data/outputs/rank_2022.csv") |> 
  select(-year)

# KEEP COMPLETE OBSERVATIONS & FIRST CLEANING ----
RANK.clean <- RANK.raw |>
  filter(complete.cases(RANK.raw)) |> 
  relocate(region, .after = name) |>
  relocate(country, .after = region) |> 
  select(-c(1:3)) |> 
  mutate(across(16:20,
                function(x) 
                {as.numeric(x)}
  )
  )

# HISTOGRAMS ----
# hist(RANK.complete$alumni, "Scott")
# hist(RANK.complete$award, "Scott")
# hist(RANK.complete$hici, "Scott")
# hist(RANK.complete$`n&s`, "Scott")
# hist(RANK.complete$pub, "Scott")
# hist(RANK.complete$pcp, "Scott")
# 
# 
# hist(RANK.complete$`academic reputation`, "Scott")
# hist(RANK.complete$`citations per faculty`, "Scott")
# hist(RANK.complete$`employer reputation`, "Scott")
# hist(RANK.complete$`faculty student`, "Scott")
# hist(RANK.complete$`international faculty`, "Scott")
# hist(RANK.complete$`international students`, "Scott")
# 
# 
# hist(RANK.complete$teaching, "Scott")
# hist(RANK.complete$citations, "Scott")
# hist(RANK.complete$industry_income, "Scott")
# hist(RANK.complete$international_outlook, "Scott")
# hist(RANK.complete$research, "Scott")

# RECODING ----
RANK.recoded <- RANK.clean |>
  mutate(
    `international faculty` = case_when(
      is.na(`international faculty`) ~ 0,
      T ~ `international faculty`
    ),
    across(
      4:6,
      function(x) {
        case_when(
          x <= 10 ~ 1,
          x > 10 & x <= 25 ~ 2,
          x > 25 & x <= 50 ~ 3,
          x > 50 & x <= 100 ~ 4,
          x > 100 & x <= 500 ~ 5,
          TRUE ~ 6
        )
      }
    ),
    awards = case_when(
      alumni == 0 & award == 0 ~ 1,
      alumni == 0 & award > 0 ~ 2,
      alumni > 0 & award == 0 ~ 3,
      alumni > 0 & award > 0 ~ 4
    ),
    awards = factor(awards),
    across(
      c(
        `academic reputation`,
        citations,
        hici,
        pub,
        `n&s`
      ),
      function(x) {
        cut(x, 3)
      }
    ),
    across(
      c(
        `international students`,
        `international faculty`,
        `employer reputation`,
        `faculty student`,
        industry_income 
      ),
      function(x) {
        cut(x, 5)
      }
    ),
    across(
      c(
        pcp,
        `citations per faculty`,
        research
      ),
      function(x) {
        cut(x,
            breaks = quantile(x, 
                              probs = c(0,0.2,0.5,0.8,1)),
            include.lowest = TRUE)
      }
    ),
    teaching = cut(teaching, 5),
    international_outlook = cut(international_outlook,
                                breaks = quantile(international_outlook,
                                                  probs = 
                                                    c(0,0.1,0.4,0.7,0.9,1)),
                                include.lowest = TRUE)
  )

# MCA ----
library(soc.ca)

RANK.act <- RANK.recoded |> 
  select(
    research,
    pcp,
    `citations per faculty`,
    teaching,
    `employer reputation`,
    industry_income,
    `international faculty`,
    `international students`,
    international_outlook
  )

RANK.sup <- RANK.recoded |> 
  select(2:6) |> 
  mutate_all(
    function(x) {
      as.factor(x)
    }
  )

RANK.id <- RANK.recoded |> 
  select(name)

MCA.res <- soc.mca(
  active = RANK.act, 
#  sup = RANK.sup[,2],
  identifier = RANK.id)

print(MCA.res)
