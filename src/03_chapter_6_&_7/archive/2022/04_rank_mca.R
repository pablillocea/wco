# LIBS ----
library(data.table)
library(tidyverse)
library(soc.ca)

# READ ----
RAW.rank <- fread("data/outputs/rank_20162020.csv")

# fix this so that it's the latest available value and Country matches country
total.pop <- fread("data/macro_indicators/total_pop.csv") |>
  # keep needed variabels
  select(
    Country, 
    Time, 
    Value
    ) |>
  # rename
  rename(
    country = Country,
    year    = Time,
    pop     = Value
    ) |>
  # keep last observation available
  group_by(country) |> 
  arrange(country,year) |> 
  slice_tail() |>  
  ungroup() |> 
  # match country names
  mutate(
    Country = case_when(
      Country == "Czechia" ~ "Czech Republic",
      Country == "United Kingdom of Great Britain and Northern Ireland" ~ "United Kingdom",
      Country == "United States of America" ~ "United States",
      T ~ Country
    )
  )

# SAMPLE ----
## Keeping all indicators. Filtering later.
SAMPLE.rank <- RAW.rank |>
  # fix variable type and blank spaces
  mutate(
    id = trimws(id)
  ) |>
  # sample year 2020
  filter(year == 2020) |>
  # dropping unnecessary variables
  select(
    4:6, # id, region, country
    9:11, # rank in arwu, qs, the
    14:19, # arwu
    22:27, # qs
    29:34 # the
  ) |>
  na.omit() |> 
  left_join(
    total.pop,
    by = c("country" = "Country")
  )

# TRANSFORM TO CATEGORICAL ----
n.cat <- 5

SAMPLE.rank.cat <- SAMPLE.rank |>
  mutate(
    across(
      4:6,
      function(x) {
        case_when(
          x <= 10 ~ "top 10",
          x > 10 & x <= 50 ~ "top 50",
          x > 50 & x <= 100 ~ "top 100",
          x > 100 & x <= 200 ~ "top 200",
          x > 200 & x <= 500 ~ "top 500",
          TRUE ~ "top 1000"
        )
      }
    )
  ) |>
  mutate(
    award_alumni = case_when(
      arwu_alumni == 0 ~ "none",
      arwu_alumni > 0 & arwu_alumni < 20 ~ "lower",
      TRUE ~ "higher"
    ),
    award_staff = case_when(
      arwu_award == 0 ~ "none",
      arwu_alumni > 0 & arwu_alumni < 20 ~ "lower",
      TRUE ~ "higher"
    ),
    highly_cited = as.character(as.numeric(cut_number(arwu_hici, n.cat))),
    nature_science = as.character(as.numeric(cut_number(arwu_ns, n.cat))),
    papers_indexed = as.character(as.numeric(cut_number(arwu_pub, n.cat))),
    production = as.character(as.numeric(cut_number(arwu_pcp, n.cat))),
    academic_reputation = as.character(as.numeric(cut_number(qs_reputation, n.cat))),
    employer_reputation = as.character(as.numeric(cut_number(qs_employer, n.cat))),
    citations_qs = as.character(as.numeric(cut_number(qs_citations, n.cat))),
    fac_stud_rat = as.character(as.numeric(cut_number(qs_faculty_student, n.cat))),
    intl_staff = as.character(as.numeric(cut_number(qs_faculty_international, n.cat))),
    intl_student = as.character(as.numeric(cut_number(qs_international_student, n.cat))),
    industry_income = as.character(as.numeric(cut_number(the_idustry_income, n.cat))),
    teaching = as.character(as.numeric(cut_number(the_teaching, n.cat))),
    citations_the = as.character(as.numeric(cut_number(the_citations, n.cat))),
    international_outlook = as.character(as.numeric(cut_number(the_international_outlook, n.cat))),
    research = as.character(as.numeric(cut_number(the_research, n.cat))),
    n_students = as.character(as.numeric(cut_number(the_students, n.cat)))
  ) |>
  select(-c(7:24)) |>
  mutate(
    across(
      9:24,
      function(x) {
        case_when(
          x == "5" ~ "highest",
          x == "4" ~ "high",
          x == "3" ~ "mid",
          x == "2" ~ "low",
          x == "1" ~ "lowest"
        )
      }
    )
  )

# MCA ----
# active <- data.frame(SAMPLE.rank.cat[ ,7:12]) # ARWU
# active <- data.frame(SAMPLE.rank.cat[ ,13:18]) # QS
# active <- data.frame(SAMPLE.rank.cat[ ,19:24]) # THE

# BEST SO FAR: production, ratio, intl staff, intl stud, industry, teaching,
# nstudents...
# active <- data.frame(SAMPLE.rank.cat[ ,c(12, 16:20, 24)])

active <- data.frame(SAMPLE.rank.cat[, c(8, # award_staff
                                         12,# productivity
                                         16:20 # ratio, intl, ii, teach
                                         )]) # n_students?
sup <- data.frame(SAMPLE.rank.cat[, 2])
id <- data.frame(SAMPLE.rank.cat[, 1])

SOC.MCA <- soc.mca(active, id)

SOC.MCA

map.ctr(SOC.MCA,
  dim = c(1, 2), ctr.dim = 1, point.shape = "variable",
  point.alpha = 1, point.fill = "black", point.color = "black",
  point.size = "freq", label = TRUE, label.repel = TRUE,
  label.alpha = 1, label.color = "black", label.size = 6,
  label.fill = NULL, map.title = "Above average modalities: dim 1", labelx = "default", labely = "default", legend = NULL
)

map.ctr(SOC.MCA,
  dim = c(1, 2), ctr.dim = 2, point.shape = "variable",
  point.alpha = 1, point.fill = "black", point.color = "black",
  point.size = "freq", label = TRUE, label.repel = TRUE,
  label.alpha = 1, label.color = "black", label.size = 6,
  label.fill = NULL, map.title = "Above average modalities: dim 2", labelx = "default", labely = "default", legend = NULL
)

map.ind(SOC.MCA,
  dim = c(1, 2), 
  point.shape = 16, 
  point.alpha = .5,
  point.fill = "darkgrey", 
  point.color = "black", 
  point.size = 3,
  label = T, 
  label.repel = T, 
  label.alpha = 1,
  label.color = "black", 
  label.size = 4, 
  label.fill = NULL,
  map.title = "Space of individuals. Axes 1 and 2", 
  labelx = "default", 
  labely = "default",
  legend = NULL
)
