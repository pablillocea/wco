# UNIVERSITIES IN CLUSTER
data.ready[,c(1,4)] |> 
  filter(name %in% filter(clust.data, clust == 1)[,1]) |> 
  select(1) |> 
  pull() |> 
  view()

# SUMMARY DESCRIPTION OF CLUSTER
res.hcpc$desc.var$category[1][[1]][,1:3]

keys <- data.ready |> left_join(clust.data, by = join_by(name == Name)) |> 
  select(
    arwu_name,
    label
  )

fwrite(keys, "keys.csv")
